---
title: "Collaborate"
date: 2020-02-01T18:00:00+01:00
draft: false
hideLastModified: true
showInMenu: true
---

## What I do

If you want to collaborate with me, I am open for the following requests:

* [Data Engineering](#data-engineering)
* [Python Mentoring](#python-mentoring)
* [Talks](#talks)

I work remotely[^fn1] because it gives me the freedom to be both productive and to take on responsibilities in my everyday life. If the project needs me to travel, I prefer to travel by train - flying should be the last resort! I am good at communicating needs and challenges as well as organising my workload autonomously. If your project needs on-site time, we can find a compromise.

I offer my work on a freelancing basis. This means you are hiring me for my expertise, not for managing me. We are going to work at eye level or not at all. I am sorry if this sounds offensive or rude, but I want to ensure a mutual understanding right from the start.

### Data Engineering

I have worked on several data-intensive backends, using technology ranging from PostgreSQL to Databricks or Kubernetes clusters. During these projects I learned from the ground up how to model large amounts of data, how to process them efficiently, and how to make them generally available to teams of data scientists. 

I also conducted original research in the field of NLP within the scope of my M.Sc. degree. In particular I was part of the [Semantic Computing Group](http://www.sc.cit-ec.uni-bielefeld.de/home/) at Bielefeld University, where I studied the applications of NLP in the context of automated argumentation, [political manifestos](https://gitlab.com/shimst3r/WOM2Vec), and [stance detection](https://gitlab.com/shimst3r/Decompositional-Approach-to-Stance-Detection-on-Twitter). Take the repos with a grain of salt, they are older than my professionell coding career. 😅

I am highly interested in interdisciplinary work, including [computational social sciences](https://en.wikipedia.org/wiki/Computational_social_science), [culturomics](https://en.wikipedia.org/wiki/Culturomics), and [digital humanities](https://en.wikipedia.org/wiki/Digital_humanities). This could include e.g.:

* [ETL](https://en.wikipedia.org/wiki/Extract,_transform,_load) pipelines for text files,
* Semantic Analysis or Topic Modelling of large or historic corpora,
* Sentiment Analysis or Stance Detection on social media.

If you are looking for a data engineer to join your team within one of the above fields, please contact me. This includes especially academic research, NGOs, and non-profit organisations.

### Python Mentoring

I love coding in [Python](https://en.wikipedia.org/wiki/Python_(programming_language)). But what I love even more is showing other people how to use the language!

If you did not know: It is great for software engineering, all kinds of research, and improving [digital](https://en.wikipedia.org/wiki/Digital_literacy) and [information literacy](https://en.wikipedia.org/wiki/Information_literacy).

Combined with my experience as a tutor and counsellor, my mentoring offering includes (not exclusively):

* Best practices regarding the language and the ecoystem,
* Transitioning support for STEM graduates to software developers or data scientists using Python,
* Professional code reviews.

I am fluent in English and German, I prefer remote mentoring (e.g. via Zoom or Google Hangouts), and offer hourly or daily billing.

### Talks

I love sharing knowledge, be it in front of an audience in form of a talk or in the classroom over several tutorial sessions. My topics of interest are machine learning, mathematics, natural language processing, Python development, and many more. 

If you want me to give a talk at your event, I require a proper code of conduct (e.g. the [PSF's CoC](https://www.python.org/psf/conduct/)). I will not participate in your event if it is obvious that you do not care for diversity.

## What I did

Have a look at my [CV](/cv.pdf). In addition, here is a list of companies and institutes I have worked with so far:

* [Bielefeld University, Faculty of Mathematics](https://www.math.uni-bielefeld.de/?lang=en)
* [Bielefeld University, Collaborative Research Center 701](https://sfb701.math.uni-bielefeld.de/about-us)
* [grandcentrix](https://grandcentrix.net/)

[^fn1]: If you or your organisation is located in Bielefeld, preferably located near public transport or within biking distance from Siegfriedplatz, I am also open for discussing office hours.
