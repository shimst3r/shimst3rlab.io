---
title: "Contact"
date: 2020-02-01T18:00:00+01:00
draft: false
hideLastModified: true
showInMenu: true
---

If you want to chat with me, work with me, have me as a speaker or teacher, you are welcome to contact me via LinkedIn or Twitter. Please consider the following:

* I won't work with you if you're accepting or reproducing antisemitism, homophobia, racism, sexism, or transphobia[^fn1].
* I won't work for you or your company if it's about animal testing, arms, conventional fintech, conventional private transport, cryptocurrencies, defense, fast fashion, industrial livestock farming, or non-renewable energy sources[^fn1].

Now that these points are out of the way, I am more than happy to hear from you. :)

[^fn1]: In no particular order
