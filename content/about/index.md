---
title: "About Me"
date: 2020-02-01T18:00:00+01:00
draft: false
hideLastModified: true
showInMenu: true
---

Hello! My name is Nils.

I am currently living in [Bielefeld, Germany](https://en.wikipedia.org/wiki/Bielefeld) and earn my money as a software engineer. I am a [queer feminist](http://queerfeminism.com/what-is-queer-feminism/), I love [Star Trek](https://en.wikipedia.org/wiki/Star_Trek) and [cyberpunk](https://en.wikipedia.org/wiki/Cyberpunk) novels. I oppose [speciesism](https://en.wikipedia.org/wiki/Speciesism) and live vegan. In addition I try to fight the [climate crisis](https://en.wikipedia.org/wiki/Climate_crisis) where possible - this includes avoiding unnecessary flights, sourcing renewable energy, and not owning a car.

!["A picture of Nils in a park during sunset."](/about_me.png)

I went to [Bielefeld University](https://en.wikipedia.org/wiki/Bielefeld_University) for both my Bachelor of Science in mathematics as well as my Master of Science in "Informatics for the Natural Sciences" (i.e. computer science). I focused on machine learning, natural language processing, and scientific computing.

My M.Sc. thesis was on "A Decompositional Approach to Stance Detection on Twitter: The Case of [#MeToo](https://twitter.com/search?q=%23MeToo)". I also conducted two research projects on "Analyzing Political Stance in Election Programmes Using Distributed Word Representations" and "Predicting Causal Lexical Relations from Word Embeddings". All three projects were done with the help of the [Semantic Computing Group](http://www.sc.cit-ec.uni-bielefeld.de/home/) at [CITEC](https://www.cit-ec.de/en).

Since May 2018 I work as a software developer. I implement backend systems and infrastructure for [Internet of Things](https://en.wikipedia.org/wiki/Internet_of_things) applications. So far I have written code in [C#](https://en.wikipedia.org/wiki/C_Sharp_(programming_language)), [Kotlin](https://en.wikipedia.org/wiki/Kotlin_(programming_language)), and [Python](https://en.wikipedia.org/wiki/Python_(programming_language)), automated deployments with [Terraform](https://en.wikipedia.org/wiki/Terraform_(software)) on [Microsoft Azure](https://en.wikipedia.org/wiki/Microsoft_Azure), used [PostgreSQL](https://en.wikipedia.org/wiki/PostgreSQL) as the main database system, and utilised both [Databricks](https://en.wikipedia.org/wiki/Databricks) and [Kubernetes](https://en.wikipedia.org/wiki/Kubernetes) clusters for computations.

Besides work, my interests include side projects (e.g. [agipy](https://gitlab.com/shimst3r/agipy)), mentoring people on [exercism](https://exercism.io), and conducting research in the field of applied [natural language processing](https://en.wikipedia.org/wiki/Natural_language_processing) with a focus on the representation of [non-binary gender](https://en.wikipedia.org/wiki/Non-binary_gender) in contemporary science fiction novels and the analysis of [political manifestos](https://en.wikipedia.org/wiki/Manifesto) and their implementation in election campaigns.

Beginning in June 2020, I will be available for freelance work! If you are interested in hiring me as a contractor for cloud-based backends, data engineering, or whatever kind of project you are implementing in Python, please check out the [Collaborate]({{< ref "/collaborate/index.md" >}}) section of my website and leave me a message.

